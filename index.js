const product = (a, b) => a * b
const quotient = (a, b) => a / b
const sum = (a, b) => a + b
const difference = (a, b) => a - b

const scalar = (a, b, fn) => a.map(element => fn(element, b))
const vector = (a, b, fn) => [fn(a[0], b[0]), fn(a[1], b[1])]
const vectorDo = (a, b, fn) => Array.isArray(a) && Array.isArray(b)
  ? vector(a, b, fn)
  : Array.isArray(a)
    ? scalar(a, b, fn)
    : Array.isArray(b)
      ? scalar(b, a, fn)
      : fn(a, b)

const multiply = (a, b) => vectorDo(a, b, product)
const divide = (a, b) => vectorDo(a, b, quotient)
const add = (a, b) => vectorDo(a, b, sum)
const subtract = (a, b) => vectorDo(a, b, difference)

module.exports = {
  do: vectorDo,
  multiply,
  divide,
  add,
  subtract
}
